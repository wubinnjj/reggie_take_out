package org.zhiwang.reggie.dto;

import lombok.Data;
import org.zhiwang.reggie.entity.Setmeal;
import org.zhiwang.reggie.entity.SetmealDish;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
