package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.Category;

public interface CategoryService extends IService<Category> {
    public void remove(Long id);
}
