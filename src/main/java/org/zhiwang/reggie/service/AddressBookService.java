package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.AddressBook;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/17 13:20
 * @Version 1.0
 */
public interface AddressBookService extends IService<AddressBook> {
}
