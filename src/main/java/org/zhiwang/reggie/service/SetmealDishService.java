package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
