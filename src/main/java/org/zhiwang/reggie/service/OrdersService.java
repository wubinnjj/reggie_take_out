package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.Orders;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/18 10:11
 * @Version 1.0
 */
public interface OrdersService extends IService<Orders> {
    /**
     * 用户下单
     * @param orders
     */
    void submit(Orders orders);
}
