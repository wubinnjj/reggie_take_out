package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
