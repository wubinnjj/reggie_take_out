package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.ShoppingCart;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/17 16:55
 * @Version 1.0
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
}
