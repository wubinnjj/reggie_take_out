package org.zhiwang.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.zhiwang.reggie.entity.DishFlavor;
import org.zhiwang.reggie.mapper.DishFlavorMapper;
import org.zhiwang.reggie.service.DishFlavorService;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
