package org.zhiwang.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.zhiwang.reggie.entity.SetmealDish;
import org.zhiwang.reggie.mapper.SetmealDishMapper;
import org.zhiwang.reggie.service.SetmealDishService;

@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
