package org.zhiwang.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.zhiwang.reggie.entity.OrderDetail;
import org.zhiwang.reggie.mapper.OrderDetailMapper;
import org.zhiwang.reggie.service.OrderDetailService;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/18 10:13
 * @Version 1.0
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {
}
