package org.zhiwang.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.zhiwang.reggie.entity.AddressBook;
import org.zhiwang.reggie.mapper.AddressBookMapper;
import org.zhiwang.reggie.service.AddressBookService;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/17 13:29
 * @Version 1.0
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
}
