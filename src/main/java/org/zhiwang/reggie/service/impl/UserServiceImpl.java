package org.zhiwang.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.zhiwang.reggie.entity.User;
import org.zhiwang.reggie.mapper.UserMapper;
import org.zhiwang.reggie.service.UserService;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
