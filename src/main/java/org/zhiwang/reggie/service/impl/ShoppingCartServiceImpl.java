package org.zhiwang.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.zhiwang.reggie.entity.ShoppingCart;
import org.zhiwang.reggie.mapper.ShoppingCartMapper;
import org.zhiwang.reggie.service.ShoppingCartService;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/17 16:56
 * @Version 1.0
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
}
