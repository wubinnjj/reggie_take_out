package org.zhiwang.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zhiwang.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {
}
