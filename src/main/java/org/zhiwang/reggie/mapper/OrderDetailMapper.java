package org.zhiwang.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.zhiwang.reggie.entity.OrderDetail;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/18 10:09
 * @Version 1.0
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}
