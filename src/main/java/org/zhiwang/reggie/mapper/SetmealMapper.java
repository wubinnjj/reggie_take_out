package org.zhiwang.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.zhiwang.reggie.entity.Setmeal;

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
