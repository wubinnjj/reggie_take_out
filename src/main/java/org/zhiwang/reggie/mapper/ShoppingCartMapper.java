package org.zhiwang.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.zhiwang.reggie.entity.ShoppingCart;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/17 16:54
 * @Version 1.0
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
}
