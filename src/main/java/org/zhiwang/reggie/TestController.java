package org.zhiwang.reggie;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class TestController {

    @RequestMapping("/")
    public  String testIndex(){
        log.info("test被调用了");
        return "my name is wu 吴斌";
    }

}
