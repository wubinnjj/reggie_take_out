package org.zhiwang.reggie.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zhiwang.reggie.service.OrderDetailService;

/**
 * @Author wubin
 * @Description 订单
 * @Date 2022/11/18 10:17
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderDetailController {

    @Autowired
    private OrderDetailService orderDetailService;


}
