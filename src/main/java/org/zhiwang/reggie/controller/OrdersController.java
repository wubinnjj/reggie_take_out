package org.zhiwang.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zhiwang.reggie.common.BaseContext;
import org.zhiwang.reggie.common.R;
import org.zhiwang.reggie.dto.OrdersDto;
import org.zhiwang.reggie.entity.OrderDetail;
import org.zhiwang.reggie.entity.Orders;
import org.zhiwang.reggie.service.OrderDetailService;
import org.zhiwang.reggie.service.OrdersService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author wubin
 * @Description TODO
 * @Date 2022/11/18 10:16
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/order")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrderDetailService orderDetailService;
    private Page<Orders> pageInfo;

    /**
     * 用户下单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {
        log.info("订单数据：{}",orders);

        ordersService.submit(orders);

        return R.success("下单成功");
    }

    /**
     * 查询订单
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page> userPage(int page, int pageSize) {
        log.info("page = {},pageSize = {}",page,pageSize);

        //构造分页构造起
        Page<Orders> pageInfo = new Page(page, pageSize);
        Page<OrdersDto> dtoPage = new Page<>();

        //构造条件构造起
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        //当前用户查询
        queryWrapper.eq(Orders::getUserId, BaseContext.getCurrentId());
        //添加排序条件
        queryWrapper.orderByDesc(Orders::getOrderTime);

        //执行查询
        ordersService.page(pageInfo,queryWrapper);

        //拷贝对象
        BeanUtils.copyProperties(pageInfo,dtoPage,"records");
        List<Orders> records = pageInfo.getRecords();

        List<OrdersDto> ordersDtos = records.stream().map((item) -> {

            OrdersDto ordersDto = new OrdersDto();

            //拷贝对象
            BeanUtils.copyProperties(item,ordersDto);

            //根据订单号查询所有订单详情数据
            LambdaQueryWrapper<OrderDetail> queryWrapper2 = new LambdaQueryWrapper<>();
            queryWrapper2.eq(OrderDetail::getOrderId, item.getNumber());
            List<OrderDetail> orderDetails = orderDetailService.list(queryWrapper2);

            if (!orderDetails.isEmpty()) {
                ordersDto.setOrderDetails(orderDetails);
            }

            return ordersDto;
        }).collect(Collectors.toList());

        dtoPage.setRecords(ordersDtos);

        return R.success(dtoPage);
    }

    /**
     * 订单明细
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String number, String beginTime, String endTime) {

        log.info("page = {},pageSize = {},number = {}, beginTime = {}, endTime = {}",page,pageSize,number,beginTime,endTime);

        Page<Orders> pageInfo = new Page<>();

        //条件查询
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Orders::getCheckoutTime);
        queryWrapper.like(number != null,Orders::getNumber,number);
        queryWrapper.ge(beginTime != null,Orders::getCheckoutTime,beginTime).le(endTime != null,Orders::getCheckoutTime,endTime);

        ordersService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }

}
